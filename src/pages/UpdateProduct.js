import {useState, useEffect, useContext} from 'react';
import {Card, Button, Col, Row, FloatingLabel, Form, Container} from 'react-bootstrap';
import {useParams, useNavigate, Link, Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductUpdate() {

	

	const{user} = useContext(UserContext)
	const navigate = useNavigate()
	const{productId} = useParams()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [category, setCategory] = useState("");
	const [stock, setStock] = useState("");

	const updateProduct = (e) => {
		e.preventDefault()

		fetch(`https://serene-escarpment-64723.herokuapp.com/products/${productId}/update`, {
			method: "PUT",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
					
					name: name,
					description: description,
					price: price,
					category: category,
					stock: stock

				})
		})
		.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {

						Swal.fire({
								title: "Updated Successfully",
								icon: "success",
								text: "returning to products page"
							})

				navigate("/allproducts")
				} else {
						Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again"
							})
				}

			})


	}


	useEffect(() => {
		

		fetch(`https://serene-escarpment-64723.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setCategory(data.category)
			setStock(data.stock)


		})

	}, [productId])



	return(

		<Container>
			  <Form onSubmit={(e) => updateProduct(e)}>
			    <Row className="mt-3 p-2">
			      <Col md>
			      <h3 className='p-4'>Update Product</h3>
			        <FloatingLabel controlId="productName" label="Product Name" className="mb-3">
			          <Form.Control size="lg" type="text" 
							  		placeholder="Enter Product Name"
							  		value={name}
							  		onChange={e => {
							  			setName(e.target.value)
							  		}}
							  		required  />
			        </FloatingLabel>

			       	  
			          <FloatingLabel controlId="description" label="Description" className="mb-3">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter Description"
							  		value={description}
							  		onChange={e => {
							  			setDescription(e.target.value)
							  		}}
							  		required  />
			          </FloatingLabel>


			        
			          <FloatingLabel controlId="price" label="price" className="mb-3">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter Price"
							  		value={price}
							  		onChange={e => {
							  			setPrice(e.target.value)
							  		}}
							  		required   />
			          </FloatingLabel>			          

			          <FloatingLabel controlId="category" label="category" className="mb-3">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter Category"
							  		value={category}
							  		onChange={e => {
							  			setCategory(e.target.value)
							  		}}
							  		required   />
			          </FloatingLabel>			          

			          <FloatingLabel controlId="Stock" label="stock" className="mb-3">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter Stock"
							  		value={stock}
							  		onChange={e => {
							  			setStock(e.target.value)
							  		}}
							  		required   />
			          </FloatingLabel>

			          <Button id="admin-btn" variant="dark" className="m-2" style={{backgroundColor:'#775144'}} type="submit" >
		   				  Update
		   				</Button>


			      </Col>
			     </Row>
			 </Form>
		  </Container>



		)






}


