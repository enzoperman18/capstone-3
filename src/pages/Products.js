import {Fragment, useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
// import coursesData from '../data/coursesData';


export default function Products() {
	

	const [products, setProducts] = useState([])

	useEffect(() => {

		fetch('https://serene-escarpment-64723.herokuapp.com/products/')
		.then(res => res.json())
		.then(data => {
			// console.log(data)


			setProducts(data.map(product => {

				return (
					<ProductCard key = {product._id} productProp={product} />
					)
			}))

		})


	}, [])

	return (

		<Fragment>	
			<h1  className="font-link">Products</h1>
			{products}
		</Fragment>

		)
}