
import {Fragment, useEffect, useState} from 'react';
import ViewAllProduct from '../components/ViewAllProducts';



export default function AllProducts() {

	


	const[viewProducts, setViewProducts] = useState([])

	useEffect(() => {
		fetch('https://serene-escarpment-64723.herokuapp.com/products/allproducts', {
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setViewProducts(data.map(viewProducts => {

				return(
					<ViewAllProduct key={viewProducts._id} viewAllProp={viewProducts} />
					)
			}))
		})


	}, [])


	return(
		

		<Fragment> 
			<h2 className="font-link">Products</h2>
			{viewProducts}
		</Fragment>

		)


}
