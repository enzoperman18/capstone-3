import {Fragment, useEffect, useState, useContext} from 'react';
import {Card, Button, Col, Row, FloatingLabel, Form, Container} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CreateProduct() {


	const{user} = useContext(UserContext)
	const header = new Headers({'Access-Control-Allow-Origin': '*'})
	const{productId} = useParams()

  	const navigate = useNavigate()
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [category, setCategory] = useState("");
	const [stock, setStock] = useState("");


	
		function createNewProduct(e) {
			e.preventDefault()

			fetch('https://serene-escarpment-64723.herokuapp.com/products/create', {
				method:"POST",
				header: header,
				headers: {
					"Content-type": "application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
						name: name,
						description: description,
						price: price,
						category: category,
						stock: stock

					})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {

						Swal.fire({
								title: "Product successfully created!",
								icon: "success"
								
							})
				navigate('/allproducts')

				} else {
						Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again"
							})
				}

			})

		}



	return(

			<Container  className="column mt-4 col-md-6" style={{backgroundColor:'#CDC5B4'}} className='font-link'>
			  <Form onSubmit={(e) => createNewProduct(e)}>
			    <Row className="g-2 mt-3">
			      <Col md>
			      <h3>Add a Product</h3>
			        
			        <FloatingLabel controlId="name" label="Product Name">
			          <Form.Control size="lg" type="text" 
							  		placeholder="Enter Product Name"
							  		value={name}
							  		onChange={e => {
							  			setName(e.target.value)
							  		}}
							  		required  />
			        </FloatingLabel>

			       	  
			          <FloatingLabel controlId="description" label="Description">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter Description"
							  		value={description}
							  		onChange={e => {
							  			setDescription(e.target.value)
							  		}}
							  		required  />
			          </FloatingLabel>


			        
			          <FloatingLabel controlId="price" label="price">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter Price"
							  		value={price}
							  		onChange={e => {
							  			setPrice(e.target.value)
							  		}}
							  		required   />
			          </FloatingLabel>			          

			          <FloatingLabel controlId="category" label="Category">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter category"
							  		value={category}
							  		onChange={e => {
							  			setCategory(e.target.value)
							  		}}
							  		required   />
			          </FloatingLabel>			          

			          <FloatingLabel controlId="stock" label="Stock">
			            <Form.Control size="lg" type="text" 
							  		placeholder="Enter Stock"
							  		value={stock}
							  		onChange={e => {
							  			setStock(e.target.value)
							  		}}
							  		required   />
			          </FloatingLabel>

			          <Button id="admin-btn" type="submit" variant="dark" className="m-2" style={{backgroundColor:'#775144'}}>
		   				  Add
		   				</Button>


			      </Col>
			     </Row>
			 </Form>
		  </Container>
		


		)
}


