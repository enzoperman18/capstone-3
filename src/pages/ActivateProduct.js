import {useState, useEffect} from 'react';
import {Button, Col, Row, Form, Container} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function ActivateProduct() {

	

	const{productId} = useParams()

	const [isActive, setIsActive] = useState("");
	const navigate = useNavigate();

	const activate = (e) => {
		e.preventDefault()

		fetch(`https://serene-escarpment-64723.herokuapp.com/products/${productId}/activate`, {
			method: "PUT",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
					
					isActive: isActive

				})
		})
		.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {

						Swal.fire({
								title: "Updated Successfully",
								icon: "success",
								text: "Returning to products page"
							})

				navigate("/allproducts")

				} else {
						Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again"
							})
				}

			})

	}


	useEffect(() => {
		

		fetch(`https://serene-escarpment-64723.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setIsActive(data.isActive)
			


		})

	}, [productId])



	return(

		<Container  className="column mt-4 col-md-6" style={{backgroundColor:'#CDC5B4'}}>
			  <Form onSubmit={(e) => activate(e)}>
			    <Row className="g-2 mt-3">
			      <Col md>
			      <h1  className="font-link">Activate</h1>
			      	  <p>Activate this product?</p>
			          <Button id="admin-btn" variant="dark" className="m-2" style={{backgroundColor:'#775144'}} type="submit"  >
		   				  Activate
		   				</Button>

		   				

			      </Col>
			     </Row>
			 </Form>
		  </Container>



		)






}