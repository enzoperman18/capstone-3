import {useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	const { user } =useContext(UserContext);

	//Allow us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate(); //useHistory(othername)

	//The "useParams" 
	const {productId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	const order = (productId) => {

		fetch('https://serene-escarpment-64723.herokuapp.com/orders/order', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {
				Swal.fire({
					title: "Successfully Ordered",
					icon: "success",
					text: "You have successfully ordered this product."
				})

				navigate("/products")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId)

		fetch(`https://serene-escarpment-64723.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log (data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)

		})

	}, [productId])

	return (

	<Container className="mt-3">
		<Row>
			<Col lg = {{span:6, offset:3}}>
      		  <Card style={{backgroundColor:'#CDC5B4'}}>
       		     <Card.Body className="text-center">
               		 <Card.Title>{name}</Card.Title>
               		 <Card.Subtitle>Description:</Card.Subtitle>
               		 <Card.Text>{description}</Card.Text>
               		 <Card.Subtitle>Price:</Card.Subtitle>
               		 <Card.Text>PhP {price}</Card.Text>
               		 
               		 {
               		 	user.id !== null ?
               		 	<Button variant="dark" style={{backgroundColor:'#775144'}}  onClick={() => order(productId)}>Order</Button>

               		 	:

               		 	<Link className="btn btn-dark" style={{backgroundColor:'#775144'}}  to="/login"> Log in to Order</Link>
               		 }


          		  </Card.Body>
        		</Card>
       		 </Col>
        </Row>
	</Container>
		)

}