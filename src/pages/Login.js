import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

  //Allows us to consume the User context object and its properties to use for user validation
  const {user, setUser} = useContext(UserContext)

  //State hooks to store the values of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  //State to determine whether the submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  function authenticate(e){
    //Prevents page redirection via form submission
    e.preventDefault(); 

      /*
        Syntax:
          fetch('url', {options-header, body, etc.})
          .then(res => res.json())
          .then(data=> {})
      */

      localStorage.setItem("email", email);

      fetch('https://serene-escarpment-64723.herokuapp.com/users/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res => res.json())
      .then(data => {

        console.log(data)
        if(typeof data.access !== "undefined") {

          localStorage.setItem('token', data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome!"
          })
        } else {

            Swal.fire({
                title: "Authentication failed",
                icon: "error",
                text: "Check your login details and try again!"
            })
        }
      })


    //Clear input fields after submission
    setEmail('');
    setPassword('');
  }


  const retrieveUserDetails = (token) => {

    fetch('https://serene-escarpment-64723.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res=> res.json())
    .then(data => {

      console.log(data)

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })

  }



  useEffect(() => {

    if(email !== "" && password !== ""){

      setIsActive(true)

    }  else {

      setIsActive(false)

    }

  }, [email, password])


  return(   

    (user.id !== null) ?
      <Navigate to="/products/"/>

      :

<div className="p-5">
   <div className='row'>
    <img src={require('../images/login.png')} alt="pistol" className="mt-5 mt-md-0 col-md-6 d-none d-md-block"/>
    <div className="column mt-4 col-md-6" style={{backgroundColor:'#CDC5B4'}}>
      <Form className="mt-3 " onSubmit = {(e)=> authenticate(e)}>
      <h1 className= "text-center font-link">Login</h1>
        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label className="font-link">Email address</Form.Label>
          <Form.Control 
                type="email" 
                placeholder="Enter email"
                value = {email}
                onChange={e => setEmail(e.target.value)} 
                required />
          <Form.Text className="text-muted">
          We'll never share your email with anyone else.
          </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label  className="font-link">Password</Form.Label>
        <Form.Control 
              type="password" 
              placeholder="Password"
              value={password}
              onChange={e => setPassword(e.target.value)}  
              required />
      </Form.Group>

      {
        isActive ?
              <Button variant="primary" type="submit" id ="submitBtn">
                 Submit
              </Button>
              :
              <Button variant="secondary" type="submit" id ="submitBtn" disabled>
                 Submit
              </Button>

      }
      <p  className="font-link">Dont have an account yet? <a href="/register">Register Here</a></p>

    </Form>
  </div>
  </div>
  </div>

    )
}
