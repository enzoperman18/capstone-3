import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

  //State hooks to store the values of the input fields
  const header = new Headers({'Access-Control-Allow-Origin': '*'})
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  //State to determine whether the submit button is enabled or not
  const [isActive, setIsActive] = useState(false);
  const {user} = useContext(UserContext)
  const navigate = useNavigate()

  // console.log(email);
  // console.log(password1);
  // console.log(password2);

  function registerUser(e) {
    e.preventDefault()

    fetch("https://serene-escarpment-64723.herokuapp.com/users/checkEmail", {
      method : "POST",
      header: header,
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(data === true){

        Swal.fire({
          title: "Duplicate Email Found",
          icon: "error",
          text: "Kindly provide another email to complete the registration."
        })
      } else {
        fetch("https://serene-escarpment-64723.herokuapp.com/users/register", {
          method: "POST",
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            mobileNo: mobileNo,
            email: email,
            password: password1,
            verifyPassword: password2
          })
        })
        .then(res => res.json())
        .then(data => {

          console.log(data)

          if(data === true) {

            Swal.fire({
              title: "Registered Successful",
              icon: "success",
              text: "You may now log in."
            })
    setFirstName("")
    setLastName("")
    setMobileNo("")
    setEmail("");
    setPassword1("");
    setPassword2("");

    navigate("/login")


          } else {
            Swal.fire({
              title: "Something went wrong",
              icon: "error",
              text: "Please try again"
            })

          }

        })

      }

    })


  }



  useEffect(() => {

    if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "" ) && (password1 === password2)){

      setIsActive(true)

    }  else {

      setIsActive(false)

    }

  }, [firstName, lastName, mobileNo, email, password1, password2])


  return(   

      (user.id !== null) ?
      <Navigate to="/products"/>

      :

      <Container className='p-4'>
      <div className='row mt-2'>
      <img src={require('../images/register.png')} alt="hitman" className="mt-5 mt-md-0 col-md-6 d-none d-md-block"/>
      <div className= "column mt-2 mb-1 col-md-6" style={{backgroundColor:'#CDC5B4'}}>
    <Form className="mt-3" onSubmit = {(e)=> registerUser(e)}>
    <h1 className= "text-center font-link">Register</h1>
    <Form.Group className="mb-3" controlId="firstName">
      <Form.Label  className="font-link">First Name</Form.Label>
      <Form.Control 
              type="text" 
              placeholder="Enter First Name"
              value = {firstName}
              onChange={e => setFirstName(e.target.value)} 
              required />
    </Form.Group>

    <Form.Group className="mb-3" controlId="lastName">
      <Form.Label  className="font-link">Last Name</Form.Label>
      <Form.Control 
              type="text" 
              placeholder="Enter Last Name"
              value = {lastName}
              onChange={e => setLastName(e.target.value)} 
              required />
    </Form.Group>

    <Form.Group className="mb-3" controlId="mobileNo">
      <Form.Label  className="font-link">Mobile Number</Form.Label>
      <Form.Control 
              type= "Number" 
              placeholder="Enter Mobile Number"
              value = {mobileNo}
              onChange={e => setMobileNo(e.target.value)} 
              required />
    </Form.Group>

      <Form.Group className="mb-2" controlId="userEmail">
        <Form.Label  className="font-link">Email address</Form.Label>
        <Form.Control 
                type="email" 
                placeholder="Enter email"
                value = {email}
                onChange={e => setEmail(e.target.value)} 
                required />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label  className="font-link">Password</Form.Label>
        <Form.Control 
              type="password" 
              placeholder="Password"
              value={password1}
              onChange={e => setPassword1(e.target.value)}  
              required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label  className="font-link">Verify Password</Form.Label>
        <Form.Control 
              type="password" 
              placeholder="Password"
              value={password2}
              onChange={e => setPassword2(e.target.value)}  
              required />
      </Form.Group>

      {
        isActive ?
              <Button variant="primary" type="submit" id ="submitBtn">
                 Submit
              </Button>
              :
              <Button variant="secondary" type="submit" id ="submitBtn" disabled>
                 Submit
              </Button>

      }

    </Form>
      </div>
      </div>
      </Container>


    )
}
