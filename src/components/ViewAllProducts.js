import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';



export default function ViewAllProduct({viewAllProp}) {

	
	const { name, description, price, category, stock, _id, isActive} = viewAllProp;

    return (
        <Card  className="cardHighlight p-3 "style={{backgroundColor:'#CDC5B4'}}>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>ID: {_id}</Card.Text>
                <Card.Text>Active Status: {String(isActive)}</Card.Text>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>                
                <Card.Subtitle>Category</Card.Subtitle>
                <Card.Text>{category}</Card.Text>                
                <Card.Subtitle>Stock</Card.Subtitle>
                <Card.Text>{stock}</Card.Text>
                <Button variant="dark" className="m-2" style={{backgroundColor:'#775144'}} as={Link} to={`/products/update/${_id}`}>Update</Button>
                <Button variant="dark" className="m-2" style={{backgroundColor:'#775144'}} as={Link} to={`/products/${_id}/archive`}>Deactivate</Button>
                <Button variant="dark" className="m-2" style={{backgroundColor:'#775144'}} as={Link} to={`/products/${_id}/activate`}>Activate</Button>
            </Card.Body>
        </Card>
    )
}


