import {Row, Col, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner () {


	return(

			<Row>
				<Col className="p-4 font-link">
					<h1 style={{color: 'black'}}><strong>Arms Depot</strong></h1>
					<p>Premium weaponry for your arsenal</p>
					<Button  variant="dark" style={{backgroundColor:'#775144'}} as = {Link} to="/login">Get Started</Button>
				</Col>
			</Row>
		)
}