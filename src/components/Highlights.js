import {Row, Col, Card, Button}  from 'react-bootstrap'

export default function Highlights() {

	return(

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3 firearms" style={{backgroundColor:'#CDC5B4'}}>
					<Card.Body>
						<Card.Title className="font-link">Firearms</Card.Title>
							<Card.Text>
								Browse through our collection of premium firearms made for those who chase perfection.
							</Card.Text>
					</Card.Body><img src={require('../images/firearms.jpg')} alt="pistol" />
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" style={{backgroundColor:'#CDC5B4'}}>
					<Card.Body>
						<Card.Title className="font-link">Explosives</Card.Title>
							<Card.Text>
								Handcrafted explosives made for clients wanting to walk out of a Michael Bay film.
							</Card.Text>
						</Card.Body>
						<img src={require('../images/explosives.jpg')} alt="pistol" />
					</Card>
				</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" style={{backgroundColor:'#CDC5B4'}}>
					<Card.Body>
						<Card.Title className="font-link">Accessories</Card.Title>
							<Card.Text>
								We offer a wide variety of accessories for the sophisticated worker.
							</Card.Text>
					</Card.Body>
					<img src={require('../images/accessories.jpg')} alt="pistol" />
				</Card>
			</Col>
		</Row>


		)
}