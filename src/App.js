import {useState, useEffect} from 'react';
import { Container } from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import AllProducts from './pages/AllProducts';
import UpdateProduct from './pages/UpdateProduct';
import ArchiveProduct from './pages/ArchiveProduct';
import ActivateProduct from './pages/ActivateProduct';
import CreateProduct from './pages/CreateProduct';
import './App.css';
import {UserProvider} from './UserContext'; 

function App() {

  //State hook for the user state that's defined here for a global scope
  //to validate if user is logged in in our site
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  //this function is for clearing the local storage upon logging out.
  const unsetUser = () => {
    localStorage.clear();
  }

useEffect(() =>{
  fetch('https://serene-escarpment-64723.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
}
})
.then(res => res.json())
.then(data => {
    if(typeof data.id !== "undefined") {
      setUser({
        id:data._id,
        isAdmin: data.isAdmin
      })
    } else{
      setUser({
        id: null,
        isAdmin: null
      })
    }
})
}, [])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path = "/" element={<Home/>} />
            <Route path = "/products" element={<Products/>} />
            <Route path = "/products/:productId" element={<ProductView/>} />
            <Route path = "/login" element={<Login/>} />
            <Route path = "/logout" element={<Logout/>} />
            <Route path = "/register" element={<Register/>} />
            <Route path = "/allproducts" element={<AllProducts/>} />
            <Route path = "/createproduct" element={<CreateProduct/>} />
            <Route path = "/products/:productId/archive" element={<ArchiveProduct/>} />
            <Route path = "/products/:productId/activate" element={<ActivateProduct/>} />
            <Route path = "/products/update/:productId" element={<UpdateProduct/>} />
            <Route path = "*" element={<PageNotFound/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>

  )
}

export default App;
